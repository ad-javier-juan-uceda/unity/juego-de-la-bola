﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Eruption : MonoBehaviour
{
    public GameObject stone;
    public float fireRate = 0.5f;
    //private float nextRate = 0.8f;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(ThrowStone ());
    }

    // Update is called once per frame
    void Update()
    {

    }


    IEnumerator ThrowStone()
    {
        yield return new WaitForSeconds(2.0f);

        while (true)
        {
            Instantiate(stone, transform.position, Random.rotation);
            yield return new WaitForSeconds(fireRate);
            //Destroy(stone2);
        }

    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Amigo")
        {
            //Destroy(collision.gameObject);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        print("Entraras en la zona oscura");
    }
}
